/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2ia;

/**
 *
 * @author invitado
 */
public class Nodo {
    int[][] matrizEstado;
    char tipo;
    int utilidad; //La funcion de utilidad sera igual el numero de movimientos posibles de max menos el numero de movimientos posibles de min
    int profundidad;    
    Nodo padre;
    boolean expandido;
    
    public Nodo(int[][] matrizEstado, char tipo, int utilidad, int profundidad, Nodo padre){
        this.matrizEstado = matrizEstado;
        this.tipo = tipo;
        this.utilidad = utilidad;
        this.profundidad = profundidad;
        this.padre = padre;
        this.expandido = false;
    }
    
    void setUtilidad(int utilidad){
        this.utilidad = utilidad;
    }
    
    
    int posI(){
        for (int i = 0; i < matrizEstado.length; i++) {
            for (int j = 0; j < matrizEstado[0].length; j++) {
                if(tipo == 'M'){
                    if(matrizEstado[i][j] == 3){
                        return i;
                    }
                }else{
                    if(matrizEstado[i][j] == 4){
                        return i;
                    }
                }
                
            }
            
        }
        return -1;
    }
   
    int posJ(){
        for (int i = 0; i < matrizEstado.length; i++) {
            for (int j = 0; j < matrizEstado[0].length; j++) {
                if(tipo == 'M'){
                    if(matrizEstado[i][j] == 3){
                        return j;
                    }
                }else{
                    if(matrizEstado[i][j] == 4){
                        return j;
                    }
                }
                
            }
            
        }
        return -1;
    }
    
    void modificarPos(int newI, int newJ){
        
        matrizEstado[this.posI()][this.posJ()] = 1;
        if(tipo == 'M'){
            matrizEstado[newI][newJ] = 3;
        }else{
            matrizEstado[newI][newJ] = 4;
        }
    }

    public boolean isExpandido() {
        return expandido;
    }
    
    
    
    
}
