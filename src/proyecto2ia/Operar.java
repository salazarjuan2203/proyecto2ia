/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2ia;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author invitado
 */
public class Operar {

    List<Nodo> cola;

    public Operar() {
        cola = new ArrayList<Nodo>();
    }

    void expandir(Nodo raiz, int profundidad) {
        int matrizEstado[][] = raiz.matrizEstado;
        cola.add(raiz);
        raiz.expandido = true;
        if (raiz.matrizEstado[raiz.posI() - 2][raiz.posJ() - 1] == 0
                && raiz.posI() - 2 >= 0
                && raiz.posJ() - 1 >= 0) {

            Nodo nodo = new Nodo(matrizEstado, 'm', 999999999, raiz.profundidad + 1, raiz);
            nodo.modificarPos(raiz.posI() - 2, raiz.posJ() - 1);
            cola.add(nodo);
        }

        if (raiz.matrizEstado[raiz.posI() - 2][raiz.posJ() + 1] == 0
                && raiz.posI() - 2 >= 0
                && raiz.posJ() + 1 < matrizEstado[0].length) {

            Nodo nodo = new Nodo(matrizEstado, 'm', 999999999, raiz.profundidad + 1, raiz);
            nodo.modificarPos(raiz.posI() - 2, raiz.posJ() + 1);
            cola.add(nodo);
        }

        if (raiz.matrizEstado[raiz.posI() - 1][raiz.posJ() + 2] == 0
                && raiz.posI() - 1 >= 0
                && raiz.posJ() + 2 < matrizEstado[0].length) {

            Nodo nodo = new Nodo(matrizEstado, 'm', 999999999, raiz.profundidad + 1, raiz);
            nodo.modificarPos(raiz.posI() - 1, raiz.posJ() + 2);
            cola.add(nodo);
        }

        if (raiz.matrizEstado[raiz.posI() + 1][raiz.posJ() + 2] == 0
                && raiz.posI() + 1 < matrizEstado.length
                && raiz.posJ() + 2 < matrizEstado[0].length) {

            Nodo nodo = new Nodo(matrizEstado, 'm', 999999999, raiz.profundidad + 1, raiz);
            nodo.modificarPos(raiz.posI() + 1, raiz.posJ() + 2);
            cola.add(nodo);
        }

        if (raiz.matrizEstado[raiz.posI() + 2][raiz.posJ() + 1] == 0
                && raiz.posI() + 2 < matrizEstado.length
                && raiz.posJ() + 1 < matrizEstado[0].length) {

            Nodo nodo = new Nodo(matrizEstado, 'm', 999999999, raiz.profundidad + 1, raiz);
            nodo.modificarPos(raiz.posI() + 2, raiz.posJ() + 1);
            cola.add(nodo);
        }

        if (raiz.matrizEstado[raiz.posI() + 2][raiz.posJ() - 1] == 0
                && raiz.posI() + 2 < matrizEstado.length
                && raiz.posJ() - 1 >= 0) {

            Nodo nodo = new Nodo(matrizEstado, 'm', 999999999, raiz.profundidad + 1, raiz);
            nodo.modificarPos(raiz.posI() + 2, raiz.posJ() - 1);
            cola.add(nodo);
        }

        if (raiz.matrizEstado[raiz.posI() + 1][raiz.posJ() - 2] == 0
                && raiz.posI() + 1 < matrizEstado.length
                && raiz.posJ() - 2 >= 0) {

            Nodo nodo = new Nodo(matrizEstado, 'm', 999999999, raiz.profundidad + 1, raiz);
            nodo.modificarPos(raiz.posI() + 1, raiz.posJ() - 2);
            cola.add(nodo);
        }

        if (raiz.matrizEstado[raiz.posI() - 1][raiz.posJ() - 2] == 0
                && raiz.posI() - 1 >= 0
                && raiz.posJ() - 2 >= 0) {

            Nodo nodo = new Nodo(matrizEstado, 'm', 999999999, raiz.profundidad + 1, raiz);
            nodo.modificarPos(raiz.posI() - 1, raiz.posJ() - 2);
            cola.add(nodo);
        }

        int ultimaPosicion = -1;

        for (int i = 0; i < cola.size(); i++) {
            if (!cola.get(i).expandido) {
                ultimaPosicion = i;
                break;
            }
        }
        
        char tipoExpansion = 'M';
        int infinito = 999999999;
        
        while (cola.get(ultimaPosicion).profundidad <= profundidad && ultimaPosicion != -1) {
            cola.get(ultimaPosicion).expandido = true;
            if(cola.get(ultimaPosicion).tipo == 'm'){
                tipoExpansion = 'M';
                infinito = -999999999;
            }else{
                tipoExpansion = 'm';
                infinito = 999999999;
            }
            if (cola.get(ultimaPosicion).matrizEstado[cola.get(ultimaPosicion).posI() - 2][cola.get(ultimaPosicion).posJ() - 1] == 0
                    && cola.get(ultimaPosicion).posI() - 2 >= 0
                    && cola.get(ultimaPosicion).posJ() - 1 >= 0) {

                Nodo nodo = new Nodo(matrizEstado, tipoExpansion, infinito, cola.get(ultimaPosicion).profundidad + 1, cola.get(ultimaPosicion));
                nodo.modificarPos(cola.get(ultimaPosicion).posI() - 2, cola.get(ultimaPosicion).posJ() - 1);
                cola.add(nodo);
            }

            if (cola.get(ultimaPosicion).matrizEstado[cola.get(ultimaPosicion).posI() - 2][cola.get(ultimaPosicion).posJ() + 1] == 0
                    && cola.get(ultimaPosicion).posI() - 2 >= 0
                    && cola.get(ultimaPosicion).posJ() + 1 < matrizEstado[0].length) {

                Nodo nodo = new Nodo(matrizEstado, tipoExpansion, infinito, cola.get(ultimaPosicion).profundidad + 1, cola.get(ultimaPosicion));
                nodo.modificarPos(cola.get(ultimaPosicion).posI() - 2, cola.get(ultimaPosicion).posJ() + 1);
                cola.add(nodo);
            }

            if (cola.get(ultimaPosicion).matrizEstado[cola.get(ultimaPosicion).posI() - 1][cola.get(ultimaPosicion).posJ() + 2] == 0
                    && cola.get(ultimaPosicion).posI() - 1 >= 0
                    && cola.get(ultimaPosicion).posJ() + 2 < matrizEstado[0].length) {

                Nodo nodo = new Nodo(matrizEstado, tipoExpansion, infinito, cola.get(ultimaPosicion).profundidad + 1, cola.get(ultimaPosicion));
                nodo.modificarPos(cola.get(ultimaPosicion).posI() - 1, cola.get(ultimaPosicion).posJ() + 2);
                cola.add(nodo);
            }

            if (cola.get(ultimaPosicion).matrizEstado[cola.get(ultimaPosicion).posI() + 1][cola.get(ultimaPosicion).posJ() + 2] == 0
                    && cola.get(ultimaPosicion).posI() + 1 < matrizEstado.length
                    && cola.get(ultimaPosicion).posJ() + 2 < matrizEstado[0].length) {

                Nodo nodo = new Nodo(matrizEstado, tipoExpansion, infinito, cola.get(ultimaPosicion).profundidad + 1, cola.get(ultimaPosicion));
                nodo.modificarPos(cola.get(ultimaPosicion).posI() + 1, cola.get(ultimaPosicion).posJ() + 2);
                cola.add(nodo);
            }

            if (cola.get(ultimaPosicion).matrizEstado[cola.get(ultimaPosicion).posI() + 2][cola.get(ultimaPosicion).posJ() + 1] == 0
                    && cola.get(ultimaPosicion).posI() + 2 < matrizEstado.length
                    && cola.get(ultimaPosicion).posJ() + 1 < matrizEstado[0].length) {

                Nodo nodo = new Nodo(matrizEstado, tipoExpansion, infinito, cola.get(ultimaPosicion).profundidad + 1, cola.get(ultimaPosicion));
                nodo.modificarPos(cola.get(ultimaPosicion).posI() + 2, cola.get(ultimaPosicion).posJ() + 1);
                cola.add(nodo);
            }

            if (cola.get(ultimaPosicion).matrizEstado[cola.get(ultimaPosicion).posI() + 2][cola.get(ultimaPosicion).posJ() - 1] == 0
                    && cola.get(ultimaPosicion).posI() + 2 < matrizEstado.length
                    && cola.get(ultimaPosicion).posJ() - 1 >= 0) {

                Nodo nodo = new Nodo(matrizEstado, tipoExpansion, infinito, cola.get(ultimaPosicion).profundidad + 1, cola.get(ultimaPosicion));
                nodo.modificarPos(cola.get(ultimaPosicion).posI() + 2, cola.get(ultimaPosicion).posJ() - 1);
                cola.add(nodo);
            }

            if (cola.get(ultimaPosicion).matrizEstado[cola.get(ultimaPosicion).posI() + 1][cola.get(ultimaPosicion).posJ() - 2] == 0
                    && cola.get(ultimaPosicion).posI() + 1 < matrizEstado.length
                    && cola.get(ultimaPosicion).posJ() - 2 >= 0) {

                Nodo nodo = new Nodo(matrizEstado, tipoExpansion, infinito, cola.get(ultimaPosicion).profundidad + 1, cola.get(ultimaPosicion));
                nodo.modificarPos(cola.get(ultimaPosicion).posI() + 1, cola.get(ultimaPosicion).posJ() - 2);
                cola.add(nodo);
            }

            if (cola.get(ultimaPosicion).matrizEstado[cola.get(ultimaPosicion).posI() - 1][cola.get(ultimaPosicion).posJ() - 2] == 0
                    && cola.get(ultimaPosicion).posI() - 1 >= 0
                    && cola.get(ultimaPosicion).posJ() - 2 >= 0) {

                Nodo nodo = new Nodo(matrizEstado, tipoExpansion, infinito, cola.get(ultimaPosicion).profundidad + 1, cola.get(ultimaPosicion));
                nodo.modificarPos(cola.get(ultimaPosicion).posI() - 1, cola.get(ultimaPosicion).posJ() - 2);
                cola.add(nodo);
            }

            for (int i = 0; i < cola.size(); i++) {
                if (!cola.get(i).expandido) {
                    ultimaPosicion = i;
                    break;
                }
            }
        }

    }

}
